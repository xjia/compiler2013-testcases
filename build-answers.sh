#!/bin/bash
CC=gcc

for source in `ls *.c`; do
  rm -f /tmp/program
  $CC $source -o /tmp/program 1>/dev/null 2>&1
  if [ -f /tmp/program ]; then
    cn=$(echo $source | grep -o -P '^[_a-z0-9]+')
    echo $cn
    time /tmp/program > $cn.ans
  fi
done

